![](logo.svg)
# eslint-config-modo-base

This package provides a base JS .eslintrc (without React plugins) adopted by Modo team as an extensible shared config.

## Usage

Install peer dependencies

```bash
$ npm install --save-dev \
    babel-eslint \
    eslint \
    eslint-config-prettier@^6.15.0 \
    eslint-config-standard@^16.0.2 \
    eslint-plugin-import@^2.22.1 \
    eslint-plugin-node@^11.1.0 \
    eslint-plugin-prettier@^3.1.4 \
    eslint-plugin-promise@^4.2.1 \
    eslint-plugin-standard@^5.0.0 \
    prettier@^2.2.0
```

Install package
```bash
$ npm install --save-dev git+ssh://git@gitlab.com/cescoc/eslint-config-modo-base.git
```

Add to your `.eslintrc.json`

```json
{
    "extends": [
        "modo-base"
    ]
}
```

## Development

### Commit message guidelines

Please follow [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) convention.

### Release

```bash
# create a new release tag and update the changelog
$ npx standard-version

# push release
$ git push --follow-tags
```
