module.exports = {
  "parserOptions": {
    "ecmaVersion": 2017,
    "parser": "babel-eslint",
    "experimentalDecorators": true
},
  "extends": [
    "standard",
    "plugin:prettier/recommended",
    "prettier/standard",
  ],
  "rules": {
    "prettier/prettier": "warn",
    "no-console": "warn",
  },
};
